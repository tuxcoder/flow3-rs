use bmi270::{interface::I2cInterface, AuxData, AxisData, Bmi270, Error, PwrCtrl};
use esp_println::println;
use hal::{i2c::I2C, peripherals::I2C0};
use shared_bus::{I2cProxy, XtensaMutex};

pub struct ImuHandler {
    imu: Bmi270<I2cInterface<I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>>>,
}

impl ImuHandler {
    pub fn new(i2c: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>) -> Self {
        let mut bmi270 = Bmi270::new_i2c(i2c, bmi270::I2cAddr::Default, bmi270::Burst::Max);

        /*
        if let Some(chip_id) = bmi270.get_chip_id().ok() {
            println!("imu chip id: {}", chip_id);
        }

        bmi270.init().ok();

        let pwr_ctrl = PwrCtrl {
            aux_en: false,
            gyr_en: true,
            acc_en: true,
            temp_en: false,
        };
        bmi270.set_pwr_ctrl(pwr_ctrl).ok();*/
        Self { imu: bmi270 }
    }

    pub fn acceleration(&mut self) -> Result<AxisData, Error<hal::i2c::Error, ()>> {
        self.imu.get_acc_data()
    }

    pub fn rotation(&mut self) -> Result<AxisData, Error<hal::i2c::Error, ()>> {
        self.imu.get_gyr_data()
    }

    pub fn atmospheric_pressure(&mut self) -> Result<AuxData, Error<hal::i2c::Error, ()>> {
        todo!()
    }
}
