use embassy_futures::select::{select3, Either3};
use embassy_time::{Duration, Timer};
use embedded_graphics::{
    mono_font::{ascii::FONT_10X20, MonoTextStyle},
    pixelcolor::Rgb565,
    prelude::*,
    text::Text,
};
use hal::Rng;
use static_cell::StaticCell;

use crate::{
    demo_tasks::{display_demo, imu_demo, captouch_demo, badgelink_demo},
    flow3r::{display::Display, input::InputHandler, imu::ImuHandler, badgelink::{badgenet::{BadgenetUartLeft, BadgenetUartRight}, BadgeLink}},
};

static UART_LEFT: StaticCell<BadgenetUartLeft> = StaticCell::new();
static UART_RIGHT: StaticCell<BadgenetUartRight> = StaticCell::new();

pub async fn main_menu(mut display: Display, inputs: InputHandler, mut imu: ImuHandler, mut badgelink: BadgeLink, uart0: BadgenetUartLeft, uart1: BadgenetUartRight, mut rng: &'static mut Rng) -> ! {
    let mut inputs = inputs.split();

    let mut uart0 = UART_LEFT.init(uart0);
    let mut uart1 = UART_RIGHT.init(uart1);

    let apps = ["input_test", "imu_test", "captouch_test", "badgenet_test"];
    let mut selected = 0usize;

    display
        .fill_solid(&display.bounding_box(), Rgb565::BLACK)
        .unwrap();
    let mut text = Text::with_alignment(
        apps[selected],
        Point { x: 120, y: 120 },
        MonoTextStyle::new(&FONT_10X20, Rgb565::WHITE),
        embedded_graphics::text::Alignment::Center,
    );
    text.draw(&mut display).unwrap();
    display.flush().await.unwrap();

    loop {
        match select3(
            inputs.sw1_center.wait_for_press(),
            inputs.sw1_right.wait_for_press(),
            inputs.sw1_left.wait_for_press(),
        )
        .await
        {
            Either3::First(_) => {
                (uart0, uart1, rng) = start_current_app(apps[selected], &mut display, &mut imu, &mut badgelink, uart0, uart1, rng).await;
                display
                    .fill_solid(&display.bounding_box(), Rgb565::BLACK)
                    .unwrap();
                text.draw(&mut display).unwrap();
                display.flush().await.unwrap();
            }
            Either3::Second(_) => {
                let selected_new = (apps.len() - 1).min(selected + 1);
                if selected != selected_new {
                    play_transition_animation(&mut display, apps[selected_new], &mut text, false)
                        .await;
                }
                selected = selected_new;
            }
            Either3::Third(_) => {
                let selected_new = if selected > 0 { selected - 1 } else { selected };
                if selected != selected_new {
                    play_transition_animation(&mut display, apps[selected_new], &mut text, true)
                        .await;
                }
                selected = selected_new;
            }
        }
    }
}

async fn play_transition_animation<'a>(
    display: &mut Display,
    app: &'a str,
    text: &mut Text<'a, MonoTextStyle<'a, Rgb565>>,
    direction: bool,
) {
    let mut offset = 0i32;

    while offset < text.bounding_box().size.width as i32 / 2 + 120 {
        offset += 20;
        display
            .fill_solid(&display.bounding_box(), Rgb565::BLACK)
            .unwrap();
        text.translate(Point::new(if direction { offset } else { -offset }, 0))
            .draw(display)
            .unwrap();
        display.flush().await.unwrap();
        Timer::after(Duration::from_millis(1)).await;
    }

    text.text = app;

    let mut offset = text.bounding_box().size.width as i32 / 2 + 120;

    while offset > 0 {
        offset -= 20;
        display
            .fill_solid(&display.bounding_box(), Rgb565::BLACK)
            .unwrap();
        text.translate(Point::new(if direction { -offset } else { offset }, 0))
            .draw(display)
            .unwrap();
        display.flush().await.unwrap();
        Timer::after(Duration::from_millis(1)).await;
    }
}

async fn start_current_app(app: &str, display: &mut Display, imu: &mut ImuHandler, badgelink: &mut BadgeLink,  uart0: &'static mut BadgenetUartLeft, uart1: &'static mut BadgenetUartRight, rng: &'static mut Rng) -> (&'static mut BadgenetUartLeft, &'static mut BadgenetUartRight,  &'static mut Rng) {
    match app {
        "input_test" => {display_demo(display).await; (uart0, uart1, rng)}
        "imu_test" =>{imu_demo(display, imu).await; (uart0, uart1, rng)}
        "captouch_test" => {captouch_demo(display).await; (uart0, uart1, rng)}
        //"badgenet_test" => badgelink_demo(badgelink, uart0, uart1, rng).await,
        _ => (uart0, uart1, rng),
    }
}
