use hal::{i2c::I2C, peripherals::{I2C0, UART0, UART1}, Uart, Rng};
use port_expander::{dev::max7321::Driver, Max7321};
use shared_bus::{I2cProxy, NullMutex, XtensaMutex};

pub mod badgenet;

pub struct BadgeLink {
    port_expander: Max7321<NullMutex<Driver<I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>>>>,
}

impl BadgeLink {
    pub fn new(i2c: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>) -> Self {
        let port_expander = port_expander::Max7321::new(i2c, true, true, true, false);
        Self { port_expander}
    }

    pub fn left_audio(&mut self) -> Result<(), hal::i2c::Error> {
        let mut pes = self.port_expander.split();
        pes.p3.set_low()?;
        pes.p4.set_low()
    }

    pub fn left_badgelink(&mut self) -> Result<(), hal::i2c::Error> {
        let mut pes = self.port_expander.split();
        pes.p3.set_high()?;
        pes.p4.set_high()
    }

    pub fn right_audio(&mut self) -> Result<(), hal::i2c::Error> {
        let mut pes = self.port_expander.split();
        pes.p5.set_low()?;
        pes.p6.set_low()
    }

    pub fn right_badgelink(&mut self) -> Result<(), hal::i2c::Error> {
        let mut pes = self.port_expander.split();
        pes.p5.set_high()?;
        pes.p6.set_high()
    }
}