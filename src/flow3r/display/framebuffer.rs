use core::slice;

use display_interface::DisplayError;
use embedded_graphics::pixelcolor::{
    raw::{RawU16, ToBytes},
    Rgb565,
};
use embedded_graphics_framebuf::backends::FrameBufferBackend;

use super::display_driver::DisplayDriver;

const MAX_DMA_SIZE: usize = 32736;
const DISPLAY_SIZE: usize = 115200;

pub struct DmaFrameBuffer {
    storage: &'static mut [u8; DISPLAY_SIZE],
}

impl DmaFrameBuffer {
    pub fn take() -> Self {
        Self {
            storage: frame_buffer_storage(),
        }
    }

    pub async fn draw<'a>(&self, driver: &mut DisplayDriver<'a>) -> Result<(), DisplayError> {
        driver.send_draw_mode_command().await?;
        let mut size_left = self.storage.len();
        let mut chunk_start = self.storage.as_ptr();

        while size_left > 0 {
            let chunk_size = if size_left > MAX_DMA_SIZE {
                MAX_DMA_SIZE
            } else {
                size_left
            };
            let chunk: &'static [u8] = unsafe { slice::from_raw_parts(chunk_start, chunk_size) };
            driver.draw_dma(chunk).await?;
            size_left -= chunk_size;
            chunk_start = chunk_start.wrapping_offset(chunk_size as isize);
        }
        //println!("blitted framebuffer");
        Ok(())
    }
}

impl FrameBufferBackend for DmaFrameBuffer {
    type Color = Rgb565;

    fn set(&mut self, index: usize, color: Self::Color) {
        let index = index * 2;
        self.storage[index..index + 2].clone_from_slice(&color.to_be_bytes());
    }

    fn get(&self, index: usize) -> Self::Color {
        let index = index * 2;
        let bytes: [u8; 2] = self.storage[index..index + 2]
            .split_at(core::mem::size_of::<u16>())
            .0
            .try_into()
            .unwrap();
        Rgb565::from(RawU16::new(u16::from_be_bytes(bytes)))
    }

    fn nr_elements(&self) -> usize {
        240usize * 240usize
    }
}

fn frame_buffer_storage() -> &'static mut [u8; DISPLAY_SIZE] {
    static mut BUFFER: [u8; DISPLAY_SIZE] = [0u8; DISPLAY_SIZE];
    unsafe { &mut BUFFER }
}
