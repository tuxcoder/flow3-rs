#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(async_fn_in_trait)]
#![feature(slice_as_chunks)]

mod demo_tasks;
mod flow3r;
mod runtime;

use demo_tasks::{draw_start_screen, badgelink_demo};
use embassy_executor::Spawner;
use embassy_time::{Duration, Timer};

use esp_backtrace as _;
use flow3r::{ui::main_menu::main_menu, Flow3r};
use hal::prelude::*;

use runtime::start_runtime;

#[entry]
fn runtime() -> ! {
    start_runtime()
}

#[embassy_executor::task]
async fn main(mut flow3r: Flow3r) -> ! {
    draw_start_screen(&mut flow3r.display).await;

    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(demo_tasks::leds_fade(flow3r.leds)).ok();

    //Timer::after(Duration::from_secs(3)).await;

    //badgelink_demo(&mut flow3r.badgelink, flow3r.uart0, flow3r.uart1, flow3r.rng).await;

    main_menu(flow3r.display, flow3r.inputs, flow3r.imu, flow3r.badgelink, flow3r.uart0, flow3r.uart1, flow3r.rng).await
}
