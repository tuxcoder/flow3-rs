use crate::flow3r::{captouch::CaptouchHandler, display::Display, imu::ImuHandler, badgelink::{BadgeLink, badgenet::{BadgenetUartLeft, BadgenetUartRight, start_badgenet_left, start_badgenet_right}}};
use embassy_executor::Spawner;
use embassy_futures::select::{select, select3, Either, Either3};
use embassy_net::{udp::{UdpSocket, PacketMetadata}, Ipv6Address, Stack, StaticConfigV6, Config, Ipv6Cidr};
use embassy_net_badgelink::Device;
use embassy_time::{Duration, Timer};
use embedded_graphics::{
    image::Image,
    mono_font::{ascii::FONT_6X10, MonoTextStyle},
    pixelcolor::Rgb565,
    prelude::*,
    primitives::{Circle, PrimitiveStyle, Rectangle, StyledDrawable},
    text::Text,
};
use esp_backtrace as _;
use esp_println::println;
use hal::{Rng, uart};
use heapless::Vec;
use smoltcp::wire::IpEndpoint;
use tinybmp::Bmp;

use smart_leds::SmartLedsWrite;

use crate::flow3r::input::InputHandler;

#[embassy_executor::task]
pub async fn leds_fade(mut leds: crate::flow3r::leds::Leds) -> ! {
    let mut b = (30, false);

    loop {
        leds.write(smart_leds::brightness(
            smart_leds::gamma([smart_leds::colors::VIOLET; 40].into_iter()),
            b.0,
        ))
        .expect("failed to set leds");

        Timer::after(Duration::from_millis(30)).await;
        b = crate::flow3r::leds::brightness_fade_in_out(b, 30, 0);
    }
}

pub async fn display_demo(display: &mut crate::flow3r::display::Display) {
    let input = InputHandler;
    let mut inputs = input.split();

    display.set_backlight(100).unwrap();

    let mut rect = Rectangle::with_center(Point { x: 120, y: 120 }, Size::new(50, 50));
    let rect_style = PrimitiveStyle::with_fill(Rgb565::BLUE);
    let text = Text::new(
        "move the rectangle with \n the left shoulder rocker",
        Point { x: 50, y: 160 },
        MonoTextStyle::new(&FONT_6X10, Rgb565::WHITE),
    );

    display
        .fill_solid(&display.bounding_box(), Rgb565::BLACK)
        .unwrap();
    text.draw(display).unwrap();
    rect.draw_styled(&rect_style, display).unwrap();
    display.flush().await.unwrap();

    loop {
        match select3(
            inputs.sw1_left.wait_for_press(),
            inputs.sw1_right.wait_for_press(),
            inputs.sw2_center.wait_for_press(),
        )
        .await
        {
            Either3::First(_) => move_rectangle(&mut rect, &text, display, &rect_style, -5).await,
            Either3::Second(_) => move_rectangle(&mut rect, &text, display, &rect_style, 5).await,
            Either3::Third(_) => break,
        }
    }
}

async fn move_rectangle<'a>(
    rect: &mut Rectangle,
    text: &Text<'a, MonoTextStyle<'a, Rgb565>>,
    display: &mut Display,
    rect_style: &PrimitiveStyle<Rgb565>,
    translation: i32,
) {
    display
        .fill_solid(&display.bounding_box(), Rgb565::BLACK)
        .unwrap();
    text.draw(display).unwrap();
    rect.translate_mut(Point::new(translation, 0))
        .draw_styled(rect_style, display)
        .unwrap();
    display.flush().await.unwrap();
    Timer::after(Duration::from_millis(10)).await;
}

pub async fn draw_start_screen(display: &mut Display) {
    let bmp_data = include_bytes!("../img/logo.bmp");
    let bmp = Bmp::from_slice(bmp_data).unwrap();
    display
        .fill_solid(&display.bounding_box(), Rgb565::WHITE)
        .unwrap();
    Image::with_center(&bmp, display.bounding_box().center())
        .draw(display)
        .unwrap();
    Text::with_alignment(
        "flow3-rs starting",
        Point { x: 120, y: 180 },
        MonoTextStyle::new(&FONT_6X10, Rgb565::BLACK),
        embedded_graphics::text::Alignment::Center,
    )
    .draw(display)
    .unwrap();
    display.flush().await.unwrap();
    display.set_backlight(100).unwrap();
}

pub async fn imu_demo(display: &mut Display, imu: &mut ImuHandler) {
    let input = InputHandler;
    let mut inputs = input.split();

    let circle = Circle::with_center(display.bounding_box().center(), 20);
    let circle_style = PrimitiveStyle::with_fill(Rgb565::YELLOW);

    display
        .fill_solid(&display.bounding_box(), Rgb565::BLACK)
        .unwrap();
    circle.draw_styled(&circle_style, display).unwrap();
    display.flush().await.unwrap();

    let mut offset = (0i32, 0i32);
    loop {
        match select(inputs.sw2_center.wait_for_press(), async {
            let imu_value = imu.rotation().unwrap();
            offset = (
                (-80).max(80.min(offset.0 + imu_value.x as i32)),
                (-80).max(80.min(offset.1 + imu_value.y as i32)),
            );
            display
                .fill_solid(&display.bounding_box(), Rgb565::BLACK)
                .unwrap();
            println!("offset: {}, {}", offset.0, offset.1);
            circle
                .translate(Point::new(offset.0, offset.1))
                .draw_styled(&circle_style, display)
                .unwrap();
            display.flush().await.unwrap();
            Timer::after(Duration::from_millis(20)).await;
        })
        .await
        {
            Either::First(_) => break,
            Either::Second(_) => (),
        }
    }
}

pub async fn captouch_demo(display: &mut Display) {
    let input = InputHandler;
    let mut inputs = input.split();

    let captouch = CaptouchHandler;
    let top = captouch.top_petals();
    let bot = captouch.bottom_petals();

    let circle_top_style = PrimitiveStyle::with_fill(Rgb565::YELLOW);
    let circle_bot_style = PrimitiveStyle::with_fill(Rgb565::GREEN);

    display
        .fill_solid(&display.bounding_box(), Rgb565::BLACK)
        .unwrap();
    display.flush().await.unwrap();

    loop {
        match select(inputs.sw2_center.wait_for_press(), async {
            display
                .fill_solid(&display.bounding_box(), Rgb565::BLACK)
                .unwrap();
            if top.petal0.pressed().await {
                Circle::with_center(Point::new(120, 20), 20)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(120, 20), 10)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            }
            if top.petal2.pressed().await {
                Circle::with_center(Point::new(215, 90), 20)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(215, 90), 10)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            }
            if top.petal4.pressed().await {
                Circle::with_center(Point::new(178, 200), 20)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(178, 200), 10)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            }
            if top.petal6.pressed().await {
                Circle::with_center(Point::new(61, 200), 20)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(61, 200), 10)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            }
            if top.petal8.pressed().await {
                Circle::with_center(Point::new(25, 90), 20)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(25, 90), 10)
                    .draw_styled(&circle_top_style, display)
                    .unwrap();
            }

            if bot.petal1.pressed().await {
                Circle::with_center(Point::new(178, 39), 20)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(178, 39), 10)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            }
            if bot.petal3.pressed().await {
                Circle::with_center(Point::new(215, 150), 20)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(215, 150), 10)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            }
            if bot.petal5.pressed().await {
                Circle::with_center(Point::new(120, 220), 20)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(120, 220), 10)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            }
            if bot.petal7.pressed().await {
                Circle::with_center(Point::new(24, 150), 20)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(24, 150), 10)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            }
            if bot.petal9.pressed().await {
                Circle::with_center(Point::new(61, 29), 20)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            } else {
                Circle::with_center(Point::new(61, 29), 10)
                    .draw_styled(&circle_bot_style, display)
                    .unwrap();
            }
            display.flush().await.unwrap();
            Timer::after(Duration::from_millis(50)).await;
        })
        .await
        {
            Either::First(_) => break,
            Either::Second(_) => (),
        }
    }
}

pub async fn badgelink_demo(badgelink: &mut BadgeLink, uart0: BadgenetUartLeft, uart1: BadgenetUartRight, rng: &'static mut Rng) -> (&'static mut BadgenetUartLeft, &'static mut BadgenetUartRight,  &'static mut Rng) {
    badgelink.left_badgelink().unwrap();
    badgelink.right_badgelink().unwrap();

    let addr_left = Ipv6Address::new(0xfe80, 0x00, 0x00, 0x00, 0x5686, 0x8cff, 0xfe5c, 0x278d);
    let addr_right = Ipv6Address::new(0xfe80, 0x00, 0x00, 0x00, 0x5686, 0x8cfa, 0xfe5c, 0x278d);

    let config_left = Config::ipv6_static(StaticConfigV6 {
        address: Ipv6Cidr::new(addr_left, 10),
        gateway: None,
        dns_servers: Vec::new(),
    });

    let config_right = Config::ipv6_static(StaticConfigV6 {
        address: Ipv6Cidr::new(addr_right, 10),
        gateway: None,
        dns_servers: Vec::new(),
    });


    let network_stack_left = start_badgenet_left(uart0, rng.random().into(), config_left).await;
    let network_stack_right = start_badgenet_right(uart1, rng.random().into(), config_right).await;

    Timer::after(Duration::from_millis(10)).await;

    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(listen_task(network_stack_right)).unwrap();
    spawner.spawn(send_task(network_stack_left, addr_right)).unwrap();

    loop {
        Timer::after(Duration::from_secs(10)).await;
    }
}

/* 
#[embassy_executor::task]
async fn listen_task(mut uart: BadgenetUartRight) {
    let mut data = [0u8;100];
    println!("ready to recv");
    loop {
        uart.read(&mut data).await.unwrap();
        println!("recvd");
    }
}

#[embassy_executor::task]
async fn send_task(mut uart: BadgenetUartLeft) {
    let data = [0;128];
    loop {
        uart.write_bytes(&data).unwrap();
        println!("sent");
        Timer::after(Duration::from_secs(1)).await;
    }
}
*/

#[embassy_executor::task]
async fn listen_task(network_stack: &'static Stack<Device<'static>>) {
    let mut rx_buffer_left = [0; 4096];
    let mut tx_buffer_left = [0; 4096];
    let mut rx_meta_left = [PacketMetadata::EMPTY; 16];
    let mut tx_meta_left = [PacketMetadata::EMPTY; 16];
    let mut buf_left = [0; 4096];

    let mut socket_left = UdpSocket::new(
        network_stack,
        &mut rx_meta_left,
        &mut rx_buffer_left,
        &mut tx_meta_left,
        &mut tx_buffer_left,
    );

    let config_left = network_stack.config_v6().unwrap();
    println!("config: {:?}", config_left.address);

    socket_left.bind(1234).unwrap();

    loop {
        socket_left.recv_from(&mut buf_left).await.unwrap();
        println!("data recieved");
    }
}

#[embassy_executor::task]
async fn send_task(network_stack: &'static Stack<Device<'static>>, addr_left: Ipv6Address) {
    let mut rx_buffer_right = [0; 4096];
    let mut tx_buffer_right = [0; 4096];
    let mut rx_meta_right = [PacketMetadata::EMPTY; 16];
    let mut tx_meta_right = [PacketMetadata::EMPTY; 16];

    let mut socket_right = UdpSocket::new(
        network_stack,
        &mut rx_meta_right,
        &mut rx_buffer_right,
        &mut tx_meta_right,
        &mut tx_buffer_right,
    );

    let config_right = network_stack.config_v6().unwrap();
    println!("config: {:?}", config_right.address);

    socket_right.bind(2345).unwrap();

    let endpoint = IpEndpoint::new(addr_left.into_address(), 1234);
    let send_data = [0;128];

    loop {
        socket_right.send_to(&send_data, endpoint).await.unwrap();
        println!("data sent");

        Timer::after(Duration::from_secs(1)).await;
    }

}