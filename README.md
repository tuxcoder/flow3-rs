<img align="right" src="img/logo.png" width="220" />

# flow3-rs
### Rust board-support / runtime for the flow3r badge

This repo provides a pure-rust board support crate / runtime + drivers for the hardware on the badge

> ## This is very much alpha, do not rely on completeness or stability 
> This project is still WIP, many things do not work yet, and many are subject to change.
> See the list below for what works and what doesn't.
> If you want stability and feature-completeness, you should take a look at https://docs.flow3r.garden for info on the excellent C/MicroPython firmware.
> So with that out of the way, continue reading if you want to play with Rust on your badge!

## State of implementation

- [x] Display: working with embedded-graphics and DMA, blitting still needs to go on second core for performance
- [x] LEDs
- [x] Port expanders
- [x] Input rockers
- [ ] IMU: most probably working, could not fully test due to broken IMU on my prototype -> HELP APPRECIATED FOR TESTING!
- [x] Captouch: driver ~~partially~~ mostly implemented, ~~however currently broken~~, working again, API needs some love though
- [x] BadgeLink/BadgeNet: should be mostly working now, interoperability with MicroPython FW not tested -> HELP APPRECIATED FOR TESTING!
- [ ] SD-Card: not implemented yet
- [ ] Barometer: should come mostly for free with IMU, but could not test that yet
- [ ] Audio: not implemented yet

If you are interested in implementing or testing one of the missing features, I am always happy for any help. Just open an issue or reach out via matrix (@zdmx:xatellite.space) or e-mail.

## Development setup

The hardware of the badge is based on the ESP32S3 which uses the Xtensa-Architecture. Xtensa is currently not supported in mainline LLVM/rustc, so we have to install the Espressif-forks of the toolchain.

The easiest way for that is to use the `espup` tool provided by Espressif.
https://github.com/esp-rs/espup

You can install espup with cargo, and then install the toolchain with espup:
```
cargo install espup
espup install
```

Make sure your cargo bin directory (usually `$HOME/.cargo/bin`) is in PATH.

`espup` will place a file called `export-esp.sh` in your Home-Directory which you need to source to activate the toolchain.

Additionally, you should install `cargo-espflash` for easy flashing

```
cargo install cargo-espflash
```

### Flashing

To flash from the official firmware to a Rust binary, you have to put your badge into bootloader mode by holding the left shoulder button during poweron. After a few seconds, a `Espressif USB JTAG/serial debug unit` should appear on the USB bus.
This is only nessecary when flashing from the official firmware to Rust, because the C firmware takes over the usb port.
When reflashing a Rust binary, you should not have to enter bootloader mode. 

Now you can flash your code onto the badge using `espflash` by running

```
cargo espflash flash --monitor --release
```
which also immediately attaches to stdout of the badge.
If it does not attach after flashing from the official firmware to a Rust binary, restart your badge and try the flashing again.

The `--release` flag is optional, however some things (especially badgenet) might not work properly without it due to timing problems without optimizations.

## Further documentation / Apidocs

TBD
